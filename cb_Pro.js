// function declaration
function foo() {
    console.log("foo from chetan no from ankit")
    console.log("its foo from chetan")
}

//function invocation
// foo()



function bar(cb) {

    setTimeout(() => {
        console.log("bar");
        cb();
    }, 2000);

}

//callback
// bar(foo)




//converting cb to promises
function barPromise() {
    return new Promise(function (resolve, reject) {
        bar(function(){
            resolve()
        })
    })
}

//calling Promise
// barPromise().then(data=>console.log("promise completed"));


//converting Promise to async await
async function showBlocking(){
    await barPromise();
    console.log("after await")
}


showBlocking()
